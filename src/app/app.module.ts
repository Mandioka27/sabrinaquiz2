import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {NgbModule, NgbModalModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ImageComponent } from './image/image.component';
import { QuizModalComponent } from './quiz-modal/quiz-modal.component';
import { QuizService } from './quiz.service';
import { BackgroundDirective } from './background.directive';

@NgModule({
  imports: [
    BrowserModule,
    NgbModule, 
  ],
  declarations: [
    AppComponent,
    ImageComponent,
    QuizModalComponent,
    BackgroundDirective,
  ],
  providers: [QuizService],
  bootstrap: [AppComponent]
})
export class AppModule { }
