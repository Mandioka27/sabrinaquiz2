import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {

  @Input() text: string = "";
  @Input() description: string = "";
  @Input() source:string = "";
  @Output() onclick = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  click(){
    this.onclick.emit();
  }

}
