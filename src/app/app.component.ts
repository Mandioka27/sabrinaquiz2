import { Component } from '@angular/core';
import {NgbModal, NgbModalRef, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'sabrina';
  modalRef : NgbModalRef;

  constructor(private modalService: NgbModal){}

  quiz(modal: any){
    this.modalRef = this.modalService.open(modal,{ size: 'lg' });
  }
}
