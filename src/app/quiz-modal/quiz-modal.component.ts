import { Component, OnInit, Output , EventEmitter, Input} from '@angular/core';
import { Quiz } from '../quiz.model';
import { QuizService } from '../quiz.service';

@Component({
  selector: 'app-quiz-modal',
  templateUrl: './quiz-modal.component.html',
  styleUrls: ['./quiz-modal.component.scss']
})
export class QuizModalComponent implements OnInit {

  @Input() title:string;
  @Input() quizIndex:number;
  @Output() onclose = new EventEmitter();

  currentQuiz:number = 0;
  answerSelected = false;
  isCorrectSelected  = false;
  quizzes:Quiz[] = []

  showResults:boolean = false;

  correctAnswers = 0;
  incorrectAnswers = 0;

  constructor(private quizService: QuizService) { }

  ngOnInit(): void {
    this.quizzes = this.quizService.getQuizzes(this.quizIndex);
    this.quizzes.forEach(q => q.answer.forEach(a => a.selected = false));
  }

  close(){
    this.onclose.emit();
  }

  onAnswer(option:any){
    this.answerSelected = true;
    this.isCorrectSelected = option.correct;
    this.quizzes[this.currentQuiz].answer.forEach(a => a.selected = false);
    option.selected = true;
  }

  next(){
    if(this.isCorrectSelected){
      this.correctAnswers++;
    }else{
      this.incorrectAnswers++;
    }

    if(this.currentQuiz == this.quizzes.length-1){
      this.showResults = true;
    }else{
      this.currentQuiz++;
      this.answerSelected = false;
      this.isCorrectSelected = false;
    }

    this.quizzes[this.currentQuiz].answer.forEach(a => a.selected = false);
  }

}
