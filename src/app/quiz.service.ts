import { Injectable } from '@angular/core';
import { Quiz } from './quiz.model';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  quizzesSabrina: Quiz[] = [
    {
      question: 'Quem é a pessoa mais bonita do universo?',
      answer: [
        { option: 'Diogo', correct: false, selected: false },
        { option: 'Yael Shelbia', correct: false, selected: false },
        { option: 'Sabrina', correct: true, selected: false },
        { option: 'Angelina Jolie', correct: false, selected: false },
      ]
    },
    {
      question: 'Quando sabrina nasceu?',
      answer: [
        { option: '17 de outrubro de 2003', correct: false, selected: false },
        { option: '14 de junho de 2002', correct: false, selected: false },
        { option: '20 de novembro de 2003', correct: false, selected: false },
        { option: '18 de setembro de 2003', correct: true, selected: false },
      ]
    },
    {
      question: 'Quantos anos sabrina tem?',
      answer: [
        { option: '14', correct: false, selected: false },
        { option: '7', correct: false, selected: false },
        { option: '18', correct: true, selected: false },
        { option: '20', correct: false, selected: false },
      ]
    },
    {
      question: 'Quantos anos sabrina tem(mentalmente)?',
      answer: [
        { option: '14', correct: false, selected: false },
        { option: '7', correct: true, selected: false },
        { option: '18', correct: false, selected: false },
        { option: '20', correct: false, selected: false },
      ]
    },
    {
      question: 'Quem é a pessoa mais incrivel do mundo?',
      answer: [
        { option: 'Sabrina', correct: false, selected: false },
        { option: 'Albert Einstein', correct: false, selected: false },
        { option: 'Katia', correct: false, selected: false },
        { option: 'Nenhuma das alternativas', correct: true, selected: false },
      ]
    },
    {
      question: 'Qual a cor favorita da Sabrina?',
      answer: [
        { option: 'Roxo', correct: false, selected: false },
        { option: 'Preto', correct: false, selected: false },
        { option: 'Azul', correct: true, selected: false },
        { option: 'Branco', correct: false, selected: false },
      ]
    },
    {
      question: 'Quantos gatos a sabrina quer ter?',
      answer: [
        { option: '1', correct: false, selected: false },
        { option: '∞', correct: true, selected: false },
        { option: '13', correct: false, selected: false },
        { option: '5', correct: false, selected: false },
      ]
    },
    {
      question: 'A Sabrina gosta de azeitona?',
      answer: [
        { option: 'Sim', correct: false, selected: false },
        { option: 'QUE NOJO!', correct: true, selected: false },
        { option: 'Não', correct: false, selected: false },
        { option: 'Claro que sim', correct: false, selected: false },
      ]
    },
    {
      question: 'A Sabrina come muito?',
      answer: [
        { option: 'Não, quase nada', correct: false, selected: false },
        { option: 'só um pouqinho', correct: false, selected: false },
        { option: 'Assustadoramente muito', correct: true, selected: false },
        { option: 'Sim', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a altura da sabrina?',
      answer: [
        { option: '1.57', correct: true, selected: false },
        { option: '1.70', correct: false, selected: false },
        { option: '1.68', correct: false, selected: false },
        { option: '1.80', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual o animal favorito da sabrina?',
      answer: [
        { option: 'Pato', correct: false, selected: false },
        { option: 'Cachorro', correct: false, selected: false },
        { option: 'Gato', correct: true, selected: false },
        { option: 'Rato', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual o filme favorito de Sabrina?',
      answer: [
        { option: 'Ratatouille', correct: false, selected: false },
        { option: 'Como Treinar Seu Dragão', correct: true, selected: false },
        { option: 'Pets', correct: false, selected: false },
        { option: 'Meu Malvado Favorito', correct: false, selected: false },
      ]
    }

  ]

  quizzesSabrina2: Quiz[] = [
    {
      question: 'Quem é a pessoa mais bonita do universo?',
      answer: [
        { option: 'Diogo', correct: false, selected: false },
        { option: 'Yael Shelbia', correct: false, selected: false },
        { option: 'Sabrina', correct: true, selected: false },
        { option: 'Angelina Jolie', correct: false, selected: false },
      ]
    },
    {
      question: 'Quando sabrina nasceu?',
      answer: [
        { option: '17 de outrubro de 2003', correct: false, selected: false },
        { option: '14 de junho de 2002', correct: false, selected: false },
        { option: '20 de novembro de 2003', correct: false, selected: false },
        { option: '18 de setembro de 2003', correct: true, selected: false },
      ]
    },
    {
      question: 'Quantos anos sabrina tem?',
      answer: [
        { option: '14', correct: false, selected: false },
        { option: '7', correct: false, selected: false },
        { option: '18', correct: true, selected: false },
        { option: '20', correct: false, selected: false },
      ]
    },
    {
      question: 'Quantos anos sabrina tem(mentalmente)?',
      answer: [
        { option: '14', correct: false, selected: false },
        { option: '7', correct: true, selected: false },
        { option: '18', correct: false, selected: false },
        { option: '20', correct: false, selected: false },
      ]
    },
    {
      question: 'Quem é a pessoa mais incrivel do mundo?',
      answer: [
        { option: 'Sabrina', correct: false, selected: false },
        { option: 'Albert Einstein', correct: false, selected: false },
        { option: 'Katia', correct: false, selected: false },
        { option: 'Nenhuma das alternativas', correct: true, selected: false },
      ]
    },
    {
      question: 'Qual a cor favorita da Sabrina?',
      answer: [
        { option: 'Roxo', correct: false, selected: false },
        { option: 'Preto', correct: false, selected: false },
        { option: 'Azul', correct: true, selected: false },
        { option: 'Branco', correct: false, selected: false },
      ]
    },
    {
      question: 'Quantos gatos a sabrina quer ter?',
      answer: [
        { option: '1', correct: false, selected: false },
        { option: '∞', correct: true, selected: false },
        { option: '13', correct: false, selected: false },
        { option: '5', correct: false, selected: false },
      ]
    },
    {
      question: 'A Sabrina gosta de azeitona?',
      answer: [
        { option: 'Sim', correct: false, selected: false },
        { option: 'QUE NOJO!', correct: true, selected: false },
        { option: 'Não', correct: false, selected: false },
        { option: 'Claro que sim', correct: false, selected: false },
      ]
    },
    {
      question: 'A Sabrina come muito?',
      answer: [
        { option: 'Não, quase nada', correct: false, selected: false },
        { option: 'só um pouqinho', correct: false, selected: false },
        { option: 'Assustadoramente muito', correct: true, selected: false },
        { option: 'Sim', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a altura da sabrina?',
      answer: [
        { option: '1.57', correct: false, selected: false },
        { option: '1.70', correct: false, selected: false },
        { option: '1.67', correct: true, selected: false },
        { option: '1.80', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual o animal favorito da sabrina?',
      answer: [
        { option: 'Macaco', correct: true, selected: false },
        { option: 'Cachorro', correct: false, selected: false },
        { option: 'Gato', correct: false, selected: false },
        { option: 'Rato', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual o filme favorito de Sabrina?',
      answer: [
        { option: 'Ratatouille', correct: false, selected: false },
        { option: 'Como Treinar Seu Dragão', correct: true, selected: false },
        { option: 'Pets', correct: false, selected: false },
        { option: 'Meu Malvado Favorito', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a comida favorita de Sabrina?',
      answer: [
        { option: 'Macarronada', correct: false, selected: false },
        { option: 'Lasanha', correct: true, selected: false },
        { option: 'Strogonoff', correct: false, selected: false },
        { option: 'Sopa', correct: false, selected: false },
      ]
    },
    {
      question: 'Quais as estações preferidas de Sabrina?',
      answer: [
        { option: 'Verão', correct: false, selected: false },
        { option: 'Outono e Inverno', correct: false, selected: false },
        { option: 'Primavera e inverno', correct: true, selected: false },
        { option: 'Outono e primavera', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual refeição não pode faltar no dia de Sabrina?',
      answer: [
        { option: 'Almoço', correct: false, selected: false },
        { option: 'Jantar', correct: false, selected: false },
        { option: 'Café da tarde', correct: false, selected: false },
        { option: 'Café da manhã', correct: true, selected: false },
      ]
    },
    {
      question: 'Qual o nome completo de Sabrina?',
      answer: [
        { option: 'Sabrina Victoria Malagoli', correct: false, selected: false },
        { option: 'Sabrina Victhoria Malagoli', correct: false, selected: false },
        { option: 'Sabrina Victhória Malagoli', correct: true, selected: false },
        { option: 'Sabrina Victória Malagoli', correct: false, selected: false },
      ]
    },
    {
      question: 'Em que cidade Sabrina nasceu?',
      answer: [
        { option: 'São Paulo', correct: true, selected: false },
        { option: 'Campinas', correct: false, selected: false },
        { option: 'Jaguariúna', correct: false, selected: false },
        { option: 'São José dos Campos', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual desses animais Sabrina nunca teve?',
      answer: [
        { option: 'Peixe', correct: false, selected: false },
        { option: 'Calopsita', correct: false, selected: false },
        { option: 'Gato', correct: false, selected: false },
        { option: 'Cachorro', correct: true, selected: false },
      ]
    },
    {
      question: 'Qual esporte Sabrina não gosta?',
      answer: [
        { option: 'Vôlei', correct: false, selected: false },
        { option: 'Basquete', correct: true, selected: false },
        { option: 'Futebol', correct: false, selected: false },
        { option: 'Handebol', correct: false, selected: false },
      ]
    },
    {
      question: 'Que instrumento Sabrina mais gostaria de aprender?',
      answer: [
        { option: 'Harpa', correct: true, selected: false },
        { option: 'Ukulele', correct: false, selected: false },
        { option: 'Violino', correct: false, selected: false },
        { option: 'Berimbau', correct: true, selected: false },

      ]
  }];

  quizzesDiogo: Quiz[] = [
    {
      question: 'Qual a comida favorita de Diogo?',
      answer: [
        { option: 'Risoto de queijo', correct: false, selected: false },
        { option: 'Churrasco', correct: false, selected: false },
        { option: 'Risoto de frutos do mar', correct: true, selected: false },
        { option: 'Comida japonesa', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a area de física favorita de Diogo?',
      answer: [
        { option: 'Astrologia', correct: false, selected: false },
        { option: 'Fisica de particulas', correct: true, selected: false },
        { option: 'Física Nuclear', correct: false, selected: false },
        { option: 'Astronomia', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a cor favorita de Diogo?',
      answer: [
        { option: 'Preto', correct: false, selected: false },
        { option: 'Verde', correct: false, selected: false },
        { option: 'Roxo', correct: false, selected: false },
        { option: 'Azul', correct: true, selected: false },
      ]
    },
    {
      question: 'Qual cor Diogo menos gosta?',
      answer: [
        { option: 'Verde', correct: true, selected: false },
        { option: 'Laranja', correct: false, selected: false },
        { option: 'Branco', correct: false, selected: false },
        { option: 'Roxo', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual o Físico favorito de Diogo?',
      answer: [
        { option: 'Isac Newton', correct: false, selected: false },
        { option: 'Erwin Schrödinger', correct: false, selected: false },
        { option: 'Max Planck', correct: true, selected: false },
        { option: 'Richard Feynman', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a altura de Diogo?',
      answer: [
        { option: '1.80', correct: false, selected: false },
        { option: '1.78', correct: true, selected: false },
        { option: '1.81', correct: false, selected: false },
        { option: '1.79', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a musica favorita de Diogo?',
      answer: [
        { option: 'Quer Voar', correct: false, selected: false },
        { option: 'Californication', correct: false, selected: false },
        { option: 'Gorila Roxo', correct: false, selected: false },
        { option: 'Scar Tissue', correct: true, selected: false },
      ]
    },
    {
      question: 'Qual o filme favorito de Diogo?',
      answer: [
        { option: 'Baby Driver', correct: false, selected: false },
        { option: 'A Família do Futuro', correct: false, selected: false },
        { option: 'Fútil e Inútil', correct: true, selected: false },
        { option: 'Lilo & Stitch ', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual o segundo filme favorito de Diogo?',
      answer: [
        { option: 'Baby Driver', correct: false, selected: false },
        { option: 'A Família do Futuro', correct: true, selected: false },
        { option: 'Fútil e Inútil', correct: false, selected: false },
        { option: 'Lilo & Stitch ', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a linguagem de programação favorita de Diogo?',
      answer: [
        { option: 'ASM x86/x64', correct: true, selected: false },
        { option: 'C++', correct: false, selected: false },
        { option: 'Python', correct: false, selected: false },
        { option: 'C#', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a linguagem de programação Diogo não sabe programar?',
      answer: [
        { option: 'SQL', correct: false, selected: false },
        { option: 'PHP', correct: true, selected: false },
        { option: 'C', correct: false, selected: false },
        { option: 'Typescript', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual o jogo favorito de Diogo?',
      answer: [
        { option: 'Forza', correct: false, selected: false },
        { option: 'Minecraft', correct: false, selected: false },
        { option: 'Thw Witcher 3', correct: true, selected: false },
        { option: 'Don`t Starve', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a lingua favorita de Diogo?',
      answer: [
        { option: 'Japonês', correct: false, selected: false },
        { option: 'Francês', correct: false, selected: false },
        { option: 'Alemão', correct: true, selected: false },
        { option: 'Russo', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual o animal favorito de Diogo?',
      answer: [
        { option: 'Gato', correct: false, selected: false },
        { option: 'Cacatua', correct: true, selected: false },
        { option: 'Cachorro(Border collie)', correct: false, selected: false },
        { option: 'Raposa', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a flor favorita de Diogo?',
      answer: [
        { option: 'Rosa Vermelha', correct: false, selected: false },
        { option: 'Rosa Branca', correct: false, selected: false },
        { option: 'Lírio', correct: false, selected: false },
        { option: 'Flor de cerejeira', correct: true, selected: false },
      ]
    },
    
  ]

  quizzesCasal: Quiz[]= [
    {
      question: 'Dia do primeiro Beijo (Diogo)?',
      answer: [
        { option: '25 de Setembro de 2019', correct: false, selected: false },
        { option: '10 de Novembro de 2019', correct: false, selected: false },
        { option: '28 de Fevereiro de 2020', correct: false, selected: false },
        { option: '15 de Novembro de 2019', correct: true, selected: false },
      ]
    },
    {
      question: 'Primeira serie inteira que assistimos juntos (Diogo)?',
      answer: [
        { option: 'Broklyn 99', correct: false, selected: false },
        { option: 'Modern Family', correct: false, selected: false },
        { option: 'Suits', correct: false, selected: false },
        { option: 'Anne with an E', correct: true, selected: false },
      ]
    },
    {
      question: 'Qual seria o nome do nosso filho(Diogo)?',
      answer: [
        { option: 'Benjamin', correct: false, selected: false },
        { option: 'Lucca', correct: false, selected: false },
        { option: 'Heitor', correct: true, selected: false },
        { option: 'Igor', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual país iriamos juntos(Diogo)?',
      answer: [
        { option: 'Suécia', correct: false, selected: false },
        { option: 'Australia', correct: false, selected: false },
        { option: 'EUA', correct: false, selected: false },
        { option: 'Áustria', correct: true, selected: false },
      ]
    },
    {
      question: 'Onde foi o primeiro encontro(Diogo)?',
      answer: [
        { option: 'Minha casa', correct: false, selected: false },
        { option: 'Trattoria', correct: false, selected: false },
        { option: 'Parque dos lagos', correct: true, selected: false },
        { option: 'Casa da Katia', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual a primeira casa que cozinhei pra você, e o que foi?(Diogo)?',
      answer: [
        { option: 'Jaguariúna- risoto', correct: false, selected: false },
        { option: 'Jaguariúna - macarrão', correct: true, selected: false },
        { option: 'Kátia- macarrão', correct: false, selected: false },
        { option: 'Posse - Risoto', correct: false, selected: false },
      ]
    },
    {
      question: 'Data do primeiro encontro (Sabrina)?',
      answer: [
        { option: '15 de Novembro de 2019', correct: false, selected: false },
        { option: '28 de Abril de 2020', correct: true, selected: false },
        { option: '17 de agosto de 2020', correct: false, selected: false },
        { option: '10 de Dezembro de 2020', correct: false, selected: false },
      ]
    },
    {
      question: 'Primeiro presente de Diogo para sabrina (Sabrina)?',
      answer: [
        { option: 'Livro', correct: true, selected: false },
        { option: 'Correntinha', correct: false, selected: false },
        { option: 'Dinheiro', correct: false, selected: false },
        { option: 'Materiais de Papelaria', correct: false, selected: false },
      ]
    },
    {
      question: 'Quando diogo de apaixonou pela sabrnia pela primeira vez (Sabrina)?',
      answer: [
        { option: '8º Ano EF', correct: false, selected: false },
        { option: '7º Ano EF', correct: true, selected: false },
        { option: '2º Ano EM', correct: false, selected: false },
        { option: '3º Ano EM', correct: false, selected: false },
      ]
    },
    {
      question: 'Qual seria o nome da nossa filha(Sabrina)?',
      answer: [
        { option: 'Maya', correct: false, selected: false },
        { option: 'Maitê', correct: false, selected: false },
        { option: 'Zoe', correct: false, selected: false },
        { option: 'Helena', correct: true, selected: false },
      ]
    },
    
    {
      question: 'Qual foi o primeiro "Evento" juntos(Sabrina)?',
      answer: [
        { option: 'Dia dos namorados', correct: false, selected: false },
        { option: 'Natal', correct: true, selected: false },
        { option: 'Aniversario de algum dos dois', correct: false, selected: false },
        { option: 'Pascoa', correct: false, selected: false },
      ]
    },
    
    {
      question: 'Quantos filhos queremos ter(Sabrina)?',
      answer: [
        { option: '4', correct: false, selected: false },
        { option: '2', correct: true, selected: false },
        { option: '1', correct: false, selected: false },
        { option: '0', correct: false, selected: false },
      ]
    },
  ]

  constructor() { }



  getQuizzes(n: number) {
    switch (n) {
      case 1:
        this.quizzesSabrina2.forEach(x => x.answer = this.shuffleArray(x.answer))
        return this.shuffleArray(this.quizzesSabrina2);
      case 2:
        this.quizzesDiogo.forEach(x => x.answer = this.shuffleArray(x.answer))
        return this.shuffleArray(this.quizzesDiogo);
      case 3:
        this.quizzesCasal.forEach(x => x.answer = this.shuffleArray(x.answer))
        return this.shuffleArray(this.quizzesCasal);
      default:
        return [];
    }
  }

  private shuffleArray(array: any[]) {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }
}
